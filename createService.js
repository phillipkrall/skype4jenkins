var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'Skype4Jenkins',
  description: 'The nodejs.org example web server.',
  script: 'C:\\Skype4Jenkins\\index.js',
  nodeOptions: [
  ]
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});

svc.install();