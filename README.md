# Skype4Jenkins
Skype4Jenkins is a standalone Koa server that jenkins can hit to send skype messages about the status.

## Setup
1. Download this package
2. Run "npm install"
3. test the app by running "startup.bat" or "node index.js"
4. Install "Post Build Action" Jenkins Plugin
5. Configure it, by adding the contents from "Jenkins.bat" to the plugins script section.
6. Either set the env variables in the script section of the "Post Build Action", or set the global env variables needed
	* JENKINS_URL : This is the URL to your Jenkins and is used by jenkins to create the BUILD_URL
	* JENKINS_PASS : The Password for the JENKINS_USER account
	* JENKINS_USER : A Jenkins User needed to get the results from the build like the status, ect.
	* SKYPE_4_JENKINS_IDS: The names of the IDs (comma separated) you want to send the notification to[^1]
	* SKYPE_4_JENKINS_URL: The User to the koa server

[^1]: You will want to keep the tags on the Skype handle ( Skype Name ). For example, "live:phillip.krall" is not the same account as "phillip.krall". Their information is the same, but we cannot send messages to build.


If you choose to not use my Jenkins.bat script, then you will not need the enviroment variables.

## Configuration
There is a settings.json.example file with all the fields you will need to setup.

## Running Skype4Jenkins on Startup
If you want to run Skype4Jenkins when a machine starts up, then run "createService.js". This creates a windows service to state the index.js file when a machine starts up.

## If you cannot find a handle for skype
If you cannot find a handle for skype, but you are friends with the person on skype, you can run the "findUsers.js" script which will log out all the configured skype user's contacts.

## Output
There are currently two types of messages that you will see.
1. {JOBNAME} - {BUILD NUMBER} {Status} it took 00 hrs 00 minutes 00.000 seconds
2. {JOBNAME> - {BUILD NUMBER} Changes:
    - {Commit Message} [ {Committer Name} ]

## Other Stuff
- koaBody.js : This is not a file I created.. I found it at https://github.com/dlau/koa-body/blob/master/index.js
- Unparsed : This is not a file I created.. I found it at https://github.com/dlau/koa-body/blob/master/unparsed.js


## Issues
Skype has a hire security setting that will force us to login the website from time to time. If we do not do this, then no messages will be sent and you may see an error about "Could not find T".

