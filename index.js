const Koa = require('koa');
const Router = require('koa-router');
const koaBody = require('./koaBody.js');
const winston = require('winston');
const moment = require('moment');
const settings = require('./settings.json');
const skype = require('./skype.js');

const logger = winston.createLogger({
  level: settings.log_level,
  transports: [
    new winston.transports.Console({'timestamp':true}),
    new winston.transports.File({ filename: settings.log_location,'timestamp':true })
  ]
});

const app = new Koa();
const router = new Router();

/*
 * Create the endpoint to notify our skype of jenkins builds.
 */
router.post('/skype/notify',koaBody(), (ctx, next) => {
  	let body = ctx.request.body;
  	console.log(body)
    logger.info(body);
  	console.log(JSON.stringify(JSON.parse(body), null, 2));
  	logger.info('body: ' + JSON.stringify(JSON.parse(body), null, 2));
  	body = JSON.parse(body);
  	let people = body.SKYPE_4_JENKINS_IDS.split(',');
  	logger.info(`People : ${people}`);
  	duration = moment.utc(moment.duration(body.JENKINS_RESULTS.estimatedDuration).asMilliseconds()).format('HH [hrs] mm [minutes] ss.SSS [seconds]');
  	changes = body.JENKINS_RESULTS.changeSet.items;
  	changeMsg = `${body.JOB_NAME} - ${body.BUILD_DISPLAY_NAME} Changes: \n`;
  	changes.forEach((change) => {
		changeMsg += `${change.msg} [${change.author.fullName}]\n`;
  	});
  	let msg = `${body.JOB_NAME} - ${body.BUILD_DISPLAY_NAME} ${body.JENKINS_RESULTS.result} it took ${duration}`;
  	skype.sendMessage(people, msg);
  	if(changes.length > 0) {
  		skype.sendMessage(people, changeMsg);
  	}
  	ctx.body = 'All good';
});

/*
 * Run all requests through this to log everything.
*/
app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.get('X-Response-Time');
  logger.info(`Global: ${ctx.method} ${ctx.url} - ${rt}`);
});

skype.login().then((skypeAccount) => {
	logger.info('Skype Logged in');
	logger.info(JSON.stringify(skyweb.skypeAccount.selfInfo));
});
/*
 * Configure our server.
 */
app
  .use(router.routes())
//   .use(bodyParser({
//  enableTypes: ['text'],
// }))
//   .use(router.allowedMethods())
  .listen(settings.port);