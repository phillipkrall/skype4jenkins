const skyweb_1 = require("skyweb");
const settings = require('./settings.json');
var skyweb = new skyweb_1.default();

skyweb.login(settings.skype_username, settings.skype_password).then((skypeAccount) => {
    console.log('Your contacts : ' + JSON.stringify(skyweb.contactsService.contacts, null, 2));
})