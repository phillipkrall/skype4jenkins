const skyweb_1 = require("skyweb");
const settings = require('./settings.json');
var skyweb = new skyweb_1.default();

module.exports = {
	login : () => {
		return skyweb.login(settings.skype_username, settings.skype_password);
	},
	setStatus: (status) => {
		return skyweb.setStatus(status);
	},
	onMessage: (callback) => {
		skyweb.messagesCallback = function (messages) {
			callback(messages);
		};
	},
	sendMessage: (people, msg) => {
		people.forEach((person) => {
			skyweb.sendMessage(`8:${person}`, msg);
		});
	}
}